/*
  RegisterController - handles registration validation.
*/

/* global GartenApp, GartenAppUtils*/
var GartenApp = GartenApp || {};
GartenApp.RegisterController = function(options) {
    var that = new GartenAppUtils.EventPublisher,
        signupForm,
        signupValidator,
        locationAutoComplete,
        location;


    //
    //
    // INITIALIZERS


    /**
     * init - Initialize the controller.
     */
    function init() {
        location = {};
        signupForm = options.signupForm;
        signupForm[0].onsubmit = onRegisterSubmitted;
        initSignupValidation();
        options.inputNickName.onkeyup = onUsernameEntered;
        options.inputLocationName.onkeyup = onLocationTextChanged;
    }


    /**
     * initSignupValidation - Initialize the validator for the form.
     */
    function initSignupValidation() {
        signupForm.bootstrapValidator({
            framework: "bootstrap",
            feedbackIcons: {
                valid: "glyphicon glyphicon-ok",
                invalid: "glyphicon glyphicon-remove",
                validating: "glyphicon glyphicon-refresh"
            },
            fields: {
                username: {
                    validators: {
                        notEmpty: {
                            message: "The username already exists and can\'t be empty"
                        },
                        stringLength: {
                            min: 6,
                            max: 30,
                            message: "The username must be more than 6 and less than 30 characters long"
                        },
                        regexp: {
                            regexp: /^[a-zA-Z0-9_\.]+$/,
                            message: "The username can only consist of alphabetical, number, dot and underscore"
                        },
                        different: {
                            field: "password",
                            message: "The username and password can\'t be the same as each other"
                        }
                    }
                },
                email: {
                    validators: {
                        notEmpty: {
                            message: "The email address is required and can\'t be empty"
                        },
                        emailAddress: {
                            message: "The input is not a valid email address"
                        }
                    }
                },
                passWord: {
                    validators: {
                        notEmpty: {
                            message: "The password is required and can\'t be empty"
                        },
                        identical: {
                            field: "confirmPassword",
                            message: "The password and its confirm are not the same"
                        },

                        different: {
                            field: "username",
                            message: "The password can\'t be the same as username"
                        }
                    }
                },
                confirmPassword: {
                    validators: {
                        notEmpty: {
                            message: "The confirm password is required and can\'t be empty"
                        },
                        identical: {
                            field: "password",
                            message: "The password and its confirm are not the same"
                        },
                        different: {
                            field: "username",
                            message: "The password can\'t be the same as username"
                        }
                    }
                },
                locationName: {
                    message: "Please enter a location for your garden",
                    validators: {}
                }
            }
        });
        signupValidator = signupForm.data("bootstrapValidator");
    }

    //
    //
    //


    /**
     * resetForm - Delete all input in the form.
     */
    function resetForm() {
        signupForm[0].reset();
    }



    //
    //
    // CALLBACKS


    /**
     * onUsernameEntered - Called, when the user enters letters into the username field.
     */
    function onUsernameEntered() {
        var username = signupValidator.getFieldElements("username").val();
        that.notifyAll("registerUsernameEntered", username);
    }

    /**
     * onUsernameEntered - Invalidate the location field, when the user makes input there - location has to be revalidated.
     */
    function onLocationTextChanged(event) {
        if (event.key != "Enter") {
            signupValidator.updateStatus("locationName", "INVALID", "ERROR");
        }
    }


    /**
     * onLocationSelected - Set location valid and store location data.
     */
    function onLocationSelected() {
        var geoData = locationAutoComplete.getPlace();
        location.latitude = geoData.geometry.location.lat();
        location.longitude = geoData.geometry.location.lng();
        signupValidator.updateStatus("locationName", "VALID");
    }


    /**
     * onRegisterSubmitted - Called, when the user wants to submit his registration form.
     *
     * @param  {type} event Submit event of the form
     */
    function onRegisterSubmitted(event) {
        var registerData;
        if (signupValidator.isValid()) {
            registerData = parseFormArray(signupForm.serializeArray());
            registerData.location = location;
            that.notifyAll("onRegisterSubmit", registerData);
            // this will prevent the page from simply reload
        }
        event.preventDefault();
        return false;
    }

    //
    //
    // SETTERS

    /**
     * setUsernameValid - Update in validator.
     *
     * @param  {type} isValid Whether the name has already been taken.
     */

    function setUsernameValid(isValid) {
        if (isValid) {
            signupForm[0].reset();
            signupValidator.updateStatus("username", "INVALID", null);
        }
    }


    /**
     * setAutoComplete - Google Places Autocomplete has to be passed on by the main class.
     *
     * @param  {type} autoComplete The autoComplete object.
     */
    function setAutoComplete(autoComplete) {
        locationAutoComplete = autoComplete;
        locationAutoComplete.addListener("place_changed", onLocationSelected);
    }

    //
    //
    // HELPERS


    /**
     * parseFormArray - Parses the form data into a json object.
     *
     * @param  {type} formArray FormArray to be converted.
     * @return {type}           Transformed JSON object.
     */
    function parseFormArray(formArray) {
        var parsedObject = {},
            i;
        for (i = 0; i < formArray.length; i++) {
            parsedObject[formArray[i].name] = formArray[i].value;
        }
        return parsedObject;
    }

    //
    //
    //

    that.init = init;
    that.setAutoComplete = setAutoComplete;
    that.setUsernameValid = setUsernameValid;
    that.resetForm = resetForm;
    return that;
};
