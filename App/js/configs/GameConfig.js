/*
  GameConfig- File for local Asset Definition and Identification.
*/

window.GameConfig = window.GameConfig || (function() {
    var that = {};


    /**
     * get - Get config data.
     *
     * @return {type}  All config data.     
     */
    function get() {
        var config = {};
        config.plants = [{
            id: "leek",
            maxGrowthPoints: 200,
            benefitIndex: 2,
            buyValue: 10,
            sellValue: 5
        },{
            id: "bamboo",
            maxGrowthPoints: 50,
            benefitIndex: 1,
            buyValue: 5,
            sellValue: 2
        },{
            id: "cabbage",
            maxGrowthPoints: 100,
            benefitIndex: 3,
            buyValue: 3,
            sellValue: 1
        },{
            id: "carrot",
            maxGrowthPoints: 70,
            benefitIndex: 2,
            buyValue: 7,
            sellValue: 3
        },{
            id: "eggplant",
            maxGrowthPoints: 300,
            benefitIndex: 2,
            buyValue: 40,
            sellValue: 25
        },{
            id: "mushroom",
            maxGrowthPoints: 100,
            benefitIndex: 1,
            buyValue: 5,
            sellValue: 2
        },{
            id: "onion",
            maxGrowthPoints: 200,
            benefitIndex: 1,
            buyValue: 15,
            sellValue: 8
        },{
            id: "pepper",
            maxGrowthPoints: 400,
            benefitIndex: 2,
            buyValue: 35,
            sellValue: 20
        },{
            id: "potato",
            maxGrowthPoints: 400,
            benefitIndex: 3,
            buyValue: 45,
            sellValue: 23
        },{
            id: "pumpkin",
            maxGrowthPoints: 700,
            benefitIndex: 1,
            buyValue: 100,
            sellValue: 55
        },{
            id: "tomato",
            maxGrowthPoints: 200,
            benefitIndex: 3,
            buyValue: 45,
            sellValue: 23
        }];
        config.max_lifepoints = 100;

        return config;
    }

    that.get = get;
    return that;
}());
