window.APIKeys = window.APIKeys || (function() {
    var that = {},
        googlePlacesKey = "AIzaSyBHVOb3Yw9PqqPxge0pFGOk9tfDuN0xxqA";


    function getGooglePlacesKey(){
      return googlePlacesKey;
    }
    /*
      Append script to document
    */
    function loadGooglePlaces(){
        var src = "https://maps.googleapis.com/maps/api/js?key=" + googlePlacesKey + "&signed_in=true&libraries=places&callback=APIKeys.initAutocomplete",
          scriptRef;
        console.log("get places src" + src);
        scriptRef = document.createElement('script');
        scriptRef.setAttribute("type","text/javascript");
        scriptRef.setAttribute("src", src);
        scriptRef.async = true;
        scriptRef.defer = true;
        document.getElementsByTagName('body')[0].appendChild(scriptRef);
        console.log("loaded google places");
    }


    //that.initAutocomplete = initAutocomplete;
    //that.loadGooglePlaces = loadGooglePlaces;
    that.getGooglePlacesKey = getGooglePlacesKey;

    return that;
}());
