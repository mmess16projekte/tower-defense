
/**
 * GameModel - Model with all the general data for the game (player-related).
 */
/* global Game */
var Game = Game || {};
Game.GameModel = function(options) {
    var that = {},
        weather,
        temperature,
        cash,
        position,
        time;


    /**
     * init - Initialize the game mode.
     */
    function init() {
    }

    //
    //
    // GETTERS

    /**
     * getStatusViewData - Get all data to update the status view.
     *
     * @return {type}  Data for the status view bar.
     */
    function getStatusViewData() {
        return {
            weather: weather,
            temperature: temperature,
            position: position,
            cash: cash,
            time: time
        };
    }

    //
    //
    // SETTERS

    /**
     * setGameStatus - Update the current game stats (inventory, planted, weather, cash etc.)
     *
     * @param  {type} data New data.
     */
    function setGameStatus(data) {
        weather = data.weather;
        temperature = data.temperature;
        cash = data.cash;
        position = data.position;
        time = data.time;
    }

    /**
     * setCash - Change the amount of money.
     *
     * @param  {type} newCash Difference of money to be added or subracted.
     */
    function setCash(newCash) {
        cash += newCash;
    }

    //
    //
    //

    that.getStatusViewData = getStatusViewData;
    that.setCash = setCash;
    that.init = init;
    that.setGameStatus = setGameStatus;

    return that;
};
