
/**
 * PlantDictionary - A model class with all plant type information.
 */
/*global Game*/
var Game = Game || {};
Game.PlantDictionary = function(options) {
    var that = {},
        plantTypeData;

    function init() {
        plantTypeData = options.plantTypeData;
    }


    /**
     * getPlantTypeData - Get the data for a object of a certain type.
     *
     * @param  {type} id Type of the plant.
     * @return {type}    Data for the plant. (null, if not found)
     */
    function getPlantTypeData(id) {
        var i,
            data;
        for (i = 0; i < plantTypeData.length; i++) {
            data = plantTypeData[i];
            if (plantTypeData[i].id == id) {
                return data;
            }
        }

        return null;
    }

    //
    //
    //

    that.init = init;
    that.getPlantTypeData = getPlantTypeData;
    return that;
};
