/**
 * GameStatusView - Display for player related data (gardenposition, cash etc.).
 */
/* global Game, GartenAppUtils */

var Game = Game || {};
Game.GameStatusView = function(options) {
    "use strict";
    /* eslint-env browser  */
    /* global, _ */
    var that = new GartenAppUtils.EventPublisher,
        temp,
        cash,
        time,
        position,
        weatherIcon,
        btnLogout;


    /**
     * init - Initialize the Game Status View.
     */
    function init() {
        position = options.position;
        temp = options.temp;
        cash = options.cash;
        weatherIcon = options.weatherIcon;
        time = options.time;
        btnLogout = options.btnLogout;
        btnLogout.onclick = onLogout;
    }

    //
    //
    // CALLBACKS

    function onLogout(event) {
        that.notifyAll("onLogout", "LOGOUT");
    }

    //
    //
    // UPDATERS


    /**
     * updateCash - Update Cashdisplay
     *
     * @param  {type} cashValue New value.
     */
    function updateCash(cashValue) {
        cash.innerHTML = cashValue;
    }


    /**
     * updateLocation - Update Location Display
     *
     * @param  {type} city Value.
     */
    function updateLocation(city) {
        position.innerHTML = city;
    }


    /**
     * updateWeather - Update Weather Display.
     *
     * @param  {type} weatherIndex Index for the current weather in the garden.
     */
    function updateWeather(weatherIndex) {
        switch (weatherIndex) {
            case "sunny":
                weatherIcon.src = "res/weatherIcons/1.png";
                break;
            case "cloudy":
                weatherIcon.src = "res/weatherIcons/2.png";
                break;
            case "rainy":
                weatherIcon.src = "res/weatherIcons/5.png";
                break;
            case "stormy":
                weatherIcon.src = "res/weatherIcons/6.png";
                break;
            case "snowy":
                weatherIcon.src = "res/weatherIcons/9.png";
                break;
            default:
                break;
        }
    }


    /**
     * updateTemperatur - Update temperature display.
     *
     * @param  {type} currentTemp New Value.
     */
    function updateTemperatur(currentTemp) {
        temp.innerHTML = Math.round(currentTemp) + " °C ";
    }


    /**
     * updateTime - Update displayed time-
     *
     * @param  {type} currentTime New Value.
     */
    function updateTime(currentTime) {
        var hour = currentTime.getHours(),
            min = currentTime.getMinutes(),
            minFormatted = min < 10 ? "0" + min : min;
        time.innerHTML = hour + ":" + minFormatted;
    }

    //
    //
    //


    that.init = init;
    that.updateCash = updateCash;
    that.updateWeather = updateWeather;
    that.updateTime = updateTime;
    that.updateLocation = updateLocation;
    that.updateTemperatur = updateTemperatur;
    return that;
};
