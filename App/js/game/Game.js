/**
 * Game - central game module controlling display, data and input of game.
 */
/* global GartenApp, Game */
var GartenApp = GartenApp || {};
GartenApp.Game = function(options) {
    var that = new GartenAppUtils.EventPublisher,
        gameModel,
        gardenModel,
        inventoryModel,
        gameEngine,
        gameStatusView,
        inventoryView,
        gardenView,
        plantDictionary,
        // INVENTORY, if showing plantable items, STORE, if showing shop
        toolMode = "INVENTORY";


    /**
     * init - Initialize the game module.
     */
    function init() {
        initPlantDictionary();
        initModel();
        initGameEngine();
        initGameStatusView();
    }


    /**
     * initPlantDictionary - Initialize plant dictionary.
     */
    function initPlantDictionary() {
        plantDictionary = new Game.PlantDictionary({
            plantTypeData: options.config.plants
        });
        plantDictionary.init();
    }


    /**
     * initModel - Initialize Models for game, garden and inventory.
     */
    function initModel() {
        gameModel = new Game.GameModel(options);
        gameModel.init();
        gardenModel = new Game.GardenModel({
            plantDictionary: plantDictionary,
            config: options.config
        });
        gardenModel.init();
        inventoryModel = new Game.InventoryModel(options);
        inventoryModel.init();
    }


    /**
     * initGameEngine - Initialize the game engine (preload etc.)
     */
    function initGameEngine() {
        gameEngine = new Game.GameEngineFactory({
            onFinished: onLoadComplete,
            plantsList: options.config.plants
        }).getGameEngine();
    }


    /**
     * initGameViews - Initialize the garden and inventoryviews.
     */
    function initGameViews() {
        gardenView = new Game.GardenView({
            gameEngine: gameEngine,
            plantDictionary: plantDictionary,
            maxLifePoints: options.config.max_lifepoints
        });
        gardenView.init();
        gardenView.updatePlantedItems(gardenModel.getPlantedItems());

        inventoryView = new Game.InventoryView({
            gameEngine: gameEngine,
            plantsList: options.config.plants
        });
        inventoryView.init();
        inventoryView.updateInventar(inventoryModel.getInventoryItems());
        inventoryView.addEventListener("onWaterItem", onWaterItem);
        inventoryView.addEventListener("onScoopItem", onScoopItem);
        inventoryView.addEventListener("onPlantItem", onPlantItem);
        inventoryView.addEventListener("onInventarBtnClicked", onInventarBtnClicked);
        inventoryView.addEventListener("onStoreBtnClicked", onStoreBtnClicked);
        inventoryView.addEventListener("onItemSellClicked", onItemSellClicked);
        inventoryView.addEventListener("onItemBuyClicked", onItemBuyClicked);
    }

    /**
     * initGameStatusView - Initialize the statusview in the header
     */
    function initGameStatusView() {
        gameStatusView = new Game.GameStatusView({
            temp: document.getElementById("temp"),
            cash: document.getElementById("cash"),
            weatherIcon: document.getElementById("weatherIcon"),
            btnLogout: document.getElementById("logout-btn"),
            time: document.getElementById("time"),
            position: document.getElementById("location")
        });
        gameStatusView.init();
        gameStatusView.addEventListener("onLogout", onLogout);
    }

    //
    //
    // CALLBACKS


    /**
     * onLogout - Called, when the user presses the logout button.
     *
     * @param  {type} event Logout event.
     */
    function onLogout(event) {
        gameEngine.destroy();
        that.notifyAll("onLogout", {
            action: event.data
        });
    }


    /**
     * onLoadComplete - Called, when the game has preloaded successfully.
     */
    function onLoadComplete() {
        that.notifyAll("onLoadComplete", true);
        initGameViews();
    }



    /**
     * onInventarBtnClicked - Change toolbar to inventory mode.
     */
    function onInventarBtnClicked() {
        inventoryView.updateInventar(inventoryModel.getInventoryItems());
        toolMode = "INVENTORY";
    }


    /**
     * onStoreBtnClicked - Change toolbar to store mode.
     */
    function onStoreBtnClicked() {
        inventoryView.storeView(inventoryModel.getInventoryItems());
        toolMode = "STORE";
    }

    /**
     * onItemSellClicked - Validate and submit the selling of seed items.
     *
     * @param  {type} event Type data of item to be sold.
     */
    function onItemSellClicked(event) {
        var type = event.data.children[0].key
        if (inventoryModel.getItenAmount(type) > 0) {
            inventoryModel.decreaseInventoryItemAmount(type);
            gameModel.setCash(plantDictionary.getPlantTypeData(type).sellValue);
            inventoryView.storeView(inventoryModel.getInventoryItems());
            gameStatusView.updateCash(gameModel.getStatusViewData().cash);
            that.notifyAll("onAction", {
                action: "SELL",
                type: type
            });
        }
    }

    /**
     * onItemBuyClicked - Validate and submit the buying of seed items.
     *
     * @param  {type} event Type data of item to be bought.
     */
    function onItemBuyClicked(event) {
        var type = event.data.children[0].key,
            cash = gameModel.getStatusViewData().cash,
            buyValue = plantDictionary.getPlantTypeData(type).buyValue;
        if (cash >= buyValue) {
            inventoryModel.increaseInventoryItemAmount(type);
            gameModel.setCash(-buyValue);
            inventoryView.storeView(inventoryModel.getInventoryItems());
            gameStatusView.updateCash(gameModel.getStatusViewData().cash);
            that.notifyAll("onAction", {
                action: "BUY",
                type: type
            });
        } else {
            console.log("Du bist Pleite!");
        }

    }


    /**
     * onWaterItem - Update the status of the selected plant as watered.
     *
     * @param  {type} event Location of the item to be watered.
     */
    function onWaterItem(event) {
        var locationId = event.data;
        if (gardenModel.hasPlantAt(locationId)) {
            gardenModel.waterPlantAt(locationId);
            gardenView.updatePlantedItems(gardenModel.getPlantedItems());
            that.notifyAll("onAction", {
                action: "WATER",
                location: locationId
            });
        }
    }


    /**
     * onScoopItem - Removes an item- if the item is fully grown, execute harvest, else just delete the plant.
     *
     * @param  {type} event Location of the item to be scooped.
     */
    function onScoopItem(event) {
        var locationId = event.data;
        if (gardenModel.hasPlantAt(locationId)) {
            if (gardenModel.isPlantAtReady(locationId)) {
                that.notifyAll("onAction", {
                    action: "HARVEST",
                    location: locationId,
                    type: gardenModel.getTypeOfPlantAt(locationId)
                });
            } else {
                that.notifyAll("onAction", {
                    action: "REMOVE",
                    location: locationId
                });
            }
            gardenModel.removePlantAt(locationId);
            gardenView.updatePlantedItems(gardenModel.getPlantedItems());
        }
    }


    /**
     * onPlantItem - Add a new plant to the garden.
     *
     * @param  {type} event Type and desired location of the item to be planted.
     */
    function onPlantItem(event) {
        var type = event.data.type,
            location = event.data.locationId;
        if (inventoryModel.hasInventoryItemOfType(type)) {
            if (!gardenModel.hasPlantAt(location)) {
                inventoryModel.decreaseInventoryItemAmount(type);
                gardenModel.addPlantAt(location, type);
                gardenView.updatePlantedItems(gardenModel.getPlantedItems());
                inventoryView.updateInventar(inventoryModel.getInventoryItems());
                that.notifyAll("onAction", {
                    action: "PLANT",
                    location: location,
                    type: type
                });
            }
        }
    }

    //
    //
    // UPDATES


    /**
     * updateStatusData - Update the Header Bar.
     *
     * @param  {type} data New data to be assigned to the Header Bar.
     */
    function updateStatusData(data) {
        var statusData;
        gameModel.setGameStatus(data);
        statusData = gameModel.getStatusViewData();
        gameStatusView.updateTime(statusData.time);
        gameStatusView.updateCash(statusData.cash);
        gameStatusView.updateWeather(statusData.weather);
        gameStatusView.updateTemperatur(statusData.temperature);
        gameStatusView.updateLocation(statusData.position);
    }


    /**
     * updateInventory - Update the inventory items in the model and view.
     *
     * @param  {type} data New inventory data.
     */
    function updateInventory(data) {
        inventoryModel.setInventoryItems(data.inventoryItems);
        if (toolMode == "INVENTORY")
            inventoryView.updateInventar(inventoryModel.getInventoryItems());
        else if (toolMode == "STORE")
            inventoryView.storeView(inventoryModel.getInventoryItems());
    }


    /**
     * updatePlantedItems - Update the items in the garden. (model and view)
     *
     * @param  {type} data New garden data.
     */
    function updatePlantedItems(data) {
        gardenModel.setPlantedItems(data.plantedItems);
        gardenView.updatePlantedItems(gardenModel.getPlantedItems());
    }

    //
    //
    //

    that.init = init;
    that.updateStatusData = updateStatusData;
    that.updateInventory = updateInventory;
    that.updatePlantedItems = updatePlantedItems;
    return that;
};
