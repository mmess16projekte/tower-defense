/**
 * GameUtility - Utility for static functions that are used in the game.
 */
window.GameUtility = window.GameUtility || (function() {
    var that = {},
        zeroPointFields = {
            xPos: 557,
            yPos: 290
        },
        fieldSize = 32,
        // fields are square (e.g. 6*6)
        fieldNumber = 6;

    /**
     * isWithin - Check, whether a certain position lies within the gardenfields.
     *
     * @param  {type} location Location to be checked.
     * @return {type}          Whether the position lies within thegarden.
     */
    function isWithin(location) {
        return (location.xPos > zeroPointFields.xPos && location.xPos < zeroPointFields.xPos + fieldNumber * fieldSize &&
            location.yPos > zeroPointFields.yPos && location.yPos < zeroPointFields.yPos + fieldNumber * fieldSize);
    }


    /**
     * getLocationId - Get the id for a pair of x-y-values.
     *
     * @param  {type} location Position.
     * @return {type}          The locationId for the position.
     */
    function getLocationId(location) {
        var locXPos,
            locYPos,
            locationId;
        locXPos = Math.floor((location.xPos - zeroPointFields.xPos) / fieldSize);
        locYPos = Math.floor((location.yPos - zeroPointFields.yPos) / fieldSize);
        locationId = locYPos * fieldNumber + locXPos;
        return locationId;
    }


    /**
     * getLocationForId - Get x/y values for a locationid.
     *
     * @param  {type} id LocationId to be transformed
     * @return {type}    The x/y values for the id.
     */
    function getLocationForId(id) {
        return {
            x: (Math.floor((id % fieldNumber)) * fieldSize + zeroPointFields.xPos),
            y: (Math.floor((id / fieldNumber)) * fieldSize - fieldSize + zeroPointFields.yPos)
        };
    }

    //
    //
    //

    that.isWithin = isWithin;
    that.getLocationId = getLocationId;
    that.getLocationForId = getLocationForId;

    return that;
}());
