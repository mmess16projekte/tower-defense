/*
  InventoryItems: prefab to create an specify interactions of an inventory item
*/

/*global InventoryView, GartenAppUtils, GameUtility*/

var InventoryView = InventoryView || {};
InventoryView.InventoryItems = function(options) {
    var that = new GartenAppUtils.EventPublisher(),
        visualGroup,
        styleSmallText = {
            font: "16px Courier",
            fill: "#000000",
            align: "center"
        },
        callback,
        type,
        position,
        gameEngine;


    //
    //
    // INITIALIZERS

    /**
     * init - Initialize the values and appearance of an item
     */
    function init() {
        var amount = options.amount;

        type = options.type;
        position = options.position;
        callback = options.callback;
        gameEngine = options.gameEngine;
        visualGroup = gameEngine.add.group();

        if (type == "scoop" || type == "wateringCan") {
            visualGroup.add(createSprite(type, position));

        } else {
            visualGroup.add(createSprite(type, position));
            visualGroup.add(createText(amount, position));
        }
    }


    /**
     * createSprite - generate an draggable sprite
     *
     * @param  {type} type     Sprite key
     * @param  {type} position Position for the sprite
     * @return {type}          The created sprite
     */
    function createSprite(type, position) {
        var tempSprite = gameEngine.add.sprite(position.x, position.y, type);
        tempSprite.width = 40;
        tempSprite.height = 40;
        tempSprite.inputEnabled = true;
        tempSprite.input.enableDrag(false, true);
        tempSprite.events.onDragStart.add(onDragStart, this);
        tempSprite.events.onDragStop.add(onDragStop, this);
        return tempSprite;
    }


    /**
     * createText - generate text for item amount
     *
     * @param  {type} amount   item amount
     * @param  {type} position position for the text
     * @return {type}          created text
     */
    function createText(amount, position) {
        return gameEngine.make.text(position.x + 10, position.y + 45, amount, styleSmallText);
    }


    /**
     * onDragStart - Shrink item on drag start and bring it to top.
     *
     * @param  {type} sprite The sprite of the icon.
     */
    function onDragStart(sprite) {
        shrinkIcon(sprite);
        bringToFront();
    }


    /**
     * onDragStop - Tween item back, trigger actions and increase item again.
     *
     * @param  {type} sprite  The dragged sprite.
     * @param  {type} pointer Position of the mouse pointer.
     */
    function onDragStop(sprite, pointer) {
        var targetPositionId,
            pointerPosition = {
                xPos: pointer.x,
                yPos: pointer.y
            };
        if (GameUtility.isWithin(pointerPosition)) {
            targetPositionId = GameUtility.getLocationId(pointerPosition);
            callback({
                type: sprite.key,
                locationId: targetPositionId
            });
        }
        increaseIcon(sprite);
        tweenSprite(sprite);
    }


    /**
     * bringToFront - Brings this object to the top of its parents display
     */
    function bringToFront() {
        gameEngine.world.bringToTop(visualGroup);
    }


    /**
     * shrinkIcon - shrink passed sprite
     *
     * @param  {type} sprite sprite to be shrinked
     * @return {type}        description
     */
    function shrinkIcon(sprite) {
        sprite.width = 20;
        sprite.height = 20;
    }


    /**
     * increaseIcon - increase passed sprite
     *
     * @param  {type} sprite sprite to be increased.
     */
    function increaseIcon(sprite) {
        sprite.width = 40;
        sprite.height = 40;
    }


    /**
     * tweenSprite - tween sprite back to start position
     *
     * @param  {type} sprite sprite to be tweened
     */
    function tweenSprite(sprite) {
        var tween = gameEngine.add.tween(sprite);
        tween.to({
            x: position.x,
            y: position.y
        }, 500, "Quart.easeOut");
        tween.start();
    }


    /**
     * destroy - Destruktor
     *
     * @return {type}  description
     */
    function destroy() {
        visualGroup.removeAll(true);
    }

    //
    //
    // 

    that.destroy = destroy;
    that.init = init;
    return that;
};
