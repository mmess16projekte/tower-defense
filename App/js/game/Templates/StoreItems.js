/*global InventoryView, GartenAppUtils*/
/**
* StoreItems - Class for the items in the store toolbar.
*/
var InventoryView = InventoryView || {};
InventoryView.StoreItems = function(options) {
    var that = new GartenAppUtils.EventPublisher(),
        styleSmallText = {
            font: "16px Courier",
            fill: "#000000",
            align: "center"
        },
        gameEngine,
        group;
    //
    //
    // INITIALIZERS

    /**
     * init - Initialize the values and appearance of an store item
     */
    function init() {
        var xPos = options.position;

        gameEngine = options.gameEngine;
        group = gameEngine.add.group();
        group.add(createStoreSprite(options.type, xPos));
        group.add(createMinusSprite(xPos - 10));
        group.add(createPlusSprite(xPos + 25));
        group.add(createText(options.amount, xPos + 10, 655));

    }


    /**
     * createText - generate text for item amount
     *
     * @param  {type} amountValue amount of the items
     * @param  {type} position    position of the text
     * @return {type}             created text
     */
    function createText(amountValue, position) {
        var xPos = position;
        if (amountValue > 9) {
            xPos -= 7;
        }
        return gameEngine.make.text(xPos, 655, amountValue, styleSmallText);
    }


    /**
     * createStoreSprite - generate a sprite for the store
     *
     * @param  {type} type type of the sprite
     * @param  {type} xpos position of the sprite
     * @return {type}      created sprite
     */
    function createStoreSprite(type, xpos) {
        return gameEngine.add.sprite(xpos, 620, type);
    }


    /**
     * createMinusSprite - Generate a minus button for the sell function.
     *
     * @param  {type} xpos Position
     * @return {type}      Created Button
     */
    function createMinusSprite(xpos) {
        var tempBtn = gameEngine.add.button(xpos, 660, "minus", clicked, this, 2, 1, 0);
        return tempBtn;
    }


    /**
     * createPlusSprite - Generate a plus button for the buy function.
     *
     * @param  {type} xpos Position
     * @return {type}      Created Button
     */
    function createPlusSprite(xpos) {
        return gameEngine.add.button(xpos, 655, "plus", clicked, this, 2, 1, 0);
    }

    /**
     * defined bahavior of button event
     * @param  {[type]} event send callbacks so Game
     */
    function clicked(sprite) {
        switch (sprite.key) {
        case "minus":
            that.notifyAll("action", sprite);
            break;
        case "plus":
            that.notifyAll("action", sprite);
            break;
        default:
            break;
        }
    }


    /**
     * destroy - Destructor
     *
     * @return {type}  description
     */
    function destroy() {
        group.removeAll(true);
    }

    //
    //
    //

    that.destroy = destroy;
    that.init = init;
    return that;
};
