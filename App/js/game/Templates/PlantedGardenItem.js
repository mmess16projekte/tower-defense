
/*global  GameUtility, GartenAppUtils*/
/**
 * PlantedGardenItem - Class for the plant items in the garden.
 */

var GardenView = GardenView || {};
GardenView.PlantedGardenItem = function(itemData) {
    var that = new GartenAppUtils.EventPublisher(),
        visualGroup,
        styleSmallText = {
            font: "8px Courier",
            fill: "#FFFFFF",
            align: "center"
        };
    //
    //
    // INITIALIZERS

    /**
     * init - Initialize the values and appearance of planted items
     */
    function init() {
        var plantSprite,
            lifePointText,
            growthPointText,
            gameEngine,
            position;

        gameEngine = itemData.gameEngine;
        visualGroup = gameEngine.add.group();

        if (itemData.type != "") {
            position = GameUtility.getLocationForId(itemData.position);
            plantSprite = gameEngine.add.sprite(position.x, position.y, getSpriteKey(itemData.isAlive, itemData.growthIndex, itemData.type));
            // color plant blue, if it has been watered recently
            plantSprite.tint = getSpriteTint(itemData.lifeIndex);
            visualGroup.add(plantSprite);

            if (itemData.isAlive) {
                lifePointText = gameEngine.add.text(position.x, position.y + 54, "W:" + Math.round(itemData.lifeIndex*100) + "%", styleSmallText);
                visualGroup.add(lifePointText);

            }
            growthPointText = gameEngine.add.text(position.x, position.y + 32, getGrowthText(itemData.isAlive, itemData.growthIndex), styleSmallText);
            growthPointText.addColor(getTextColor(itemData.isAlive, itemData.growthIndex), 0);

            visualGroup.add(growthPointText);
        }
    }


    /**
     * destroy - Destructor
     *
     * @return {type}  description
     */
    function destroy() {
        visualGroup.removeAll(true);
    }

    /**
     * returns growth Text appropriated on
     * @param  {Boolean} isAlive     status (ready/ dead)
     * @param  {[type]}  growthIndex growth points
     * @return {[type]}  returns status in % or text if ready/dead
     */
    function getGrowthText(isAlive, growthIndex) {
        if (!isAlive) {
            return "DEAD";
        } else if (growthIndex >= 1) {
            return "READY";
        } else {
            return "G:" + Math.round(growthIndex * 100) + "%";
        }
    }

    /**
     * set tint on sprite if plant is healthy
     * @param  {[type]} lifeIndex [description]
     * @return {[type]}  returns color of tint
     */
    function getSpriteTint(lifeIndex) {
        if (lifeIndex > 90) {
            return 0x9999ff;
        } else {
            return 0xffffff;
        }
    }

    /**
     * set color on growthText appropriated on growthStatus
     * @return {[type]}  returns color growthStatus
     */
    function getTextColor(isAlive, growthIndex) {
        if (!isAlive) {
            return "#FF0000";
        } else if (growthIndex >= 1) {
            return "#45BB27";
        } else {
            return "#FFFFFF";
        }
    }


    /**
     * getSpriteKey - returns key for asset changes appropriated on growthIndex
     *
     * @param  {type} isAlive     Whether the plant is alive.
     * @param  {type} growthIndex Progress of the growth
     * @param  {type} type        Type of the plant
     * @return {type}             Sprite key
     */
    function getSpriteKey(isAlive, growthIndex, type) {
        var key;

        if (!itemData.isAlive) {
            return "dead";
        } else {
            if (growthIndex < .33) {
                key = type + 1;
            } else if (growthIndex > .66) {
                key = type + 3;
            } else {
                key = type + 2;
            }
            return key;
        }
    }

    //
    //
    //

    that.destroy = destroy;
    that.init = init;
    return that;
}
