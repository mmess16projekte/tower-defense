/**
 * InventoryView -View for the toolbar.
 */
/*global Game, InventoryView, GartenAppUtils*/
var Game = Game || {};
Game.InventoryView = function(options) {
    var that = new GartenAppUtils.EventPublisher,
        plantsList,
        gameEngine,
        inventarItems,
        inventarList = [],
        toolView = [],
        styleBigTextActive = {
            font: "24px Courier",
            fill: "#000000",
            align: "center",
            fontWeight: "bold"
        },
        styleBigText = {
            font: "24px Courier",
            fill: "#000000",
            align: "center"
        },
        inventoryText,
        storeText,
        TOOLBOXITEMS_POS_Y = 615;


    /**
     * init - Initialize the inventory view.
     */
    function init() {
        gameEngine = options.gameEngine;
        plantsList = options.plantsList;
        createToolBox();
        createMenuButtons();
    }


    /**
     * createToolBox - Init the toolbox.
     */
    function createToolBox() {
        var WATERING_CAN_POS_X = 48,
            SCOOP_POS_X = 100,
            wateringCan,
            scoop;

        wateringCan = new InventoryView.InventoryItems({
            gameEngine: gameEngine,
            type: "wateringCan",
            amount: 0,
            position: {
                x: WATERING_CAN_POS_X,
                y: TOOLBOXITEMS_POS_Y
            },
            callback: watering
        }).init();
        scoop = new InventoryView.InventoryItems({
            gameEngine: gameEngine,
            type: "scoop",
            amount: 0,
            position: {
                x: SCOOP_POS_X,
                y: TOOLBOXITEMS_POS_Y
            },
            callback: scooping
        }).init();
        toolView.push(wateringCan);
        toolView.push(scoop);
    }


    /**
     * createMenuButtons - Init the menu buttons.
     */
    function createMenuButtons() {
        gameEngine.add.button(30, 560, "inventarBtn", clicked, this, 2, 1, 0),
            gameEngine.add.button(250, 560, "storeBtn", clicked, this, 2, 1, 0);
        inventoryText = gameEngine.add.text(70, 563, "Inventory", styleBigTextActive);
        storeText = gameEngine.add.text(315, 563, "Store", styleBigText);
    }

    function clicked(sprite) {
        switch (sprite.key) {
            case "inventarBtn":
                that.notifyAll("onInventarBtnClicked");
                inventoryText.setStyle(styleBigTextActive);
                storeText.setStyle(styleBigText);
                break;
            case "storeBtn":
                that.notifyAll("onStoreBtnClicked");
                inventoryText.setStyle(styleBigText);
                storeText.setStyle(styleBigTextActive);
                break;
            default:
                break;
        }
    }


    //
    //
    //


    /**
     * updateInventar - Update the icons of the inventory.
     *
     * @param  {type} list New data.
     */
    function updateInventar(list) {
        var i,
            currentItem,
            xPos = 200;

        clearView();
        inventarItems = list;

        for (i = 0; i < inventarItems.length; i++) {
            if (inventarItems[i].amount > 0) {
                currentItem = new InventoryView.InventoryItems({
                    gameEngine: gameEngine,
                    type: inventarItems[i].type,
                    amount: inventarItems[i].amount,
                    position: {
                        x: xPos,
                        y: TOOLBOXITEMS_POS_Y
                    },
                    callback: planting
                });
                currentItem.init();
                inventarList.push(currentItem);
                xPos += 100;
            }
        }
    }


    /**
     * scooping - Callback when the scoop is used.
     *
     * @param  {type} event location to be scooped.
     */
    function scooping(event) {
        that.notifyAll("onScoopItem", event.locationId);
    }


    /**
     * watering - Callback, when a tile is watered.
     *
     * @param  {type} event location to be watered.
     */
    function watering(event) {
        that.notifyAll("onWaterItem", event.locationId);

    }


    /**
     * planting - Add a plant to a certain position.
     *
     * @param  {type} event plant type and locationId for the planting.
     */
    function planting(event) {
        that.notifyAll("onPlantItem", {
            locationId: event.locationId,
            type: event.type
        });
    }


    /**
     * clearView - Reset the toolbar items.
     */
    function clearView() {
        var i = 0;
        for (i; i < inventarList.length; i++) {
            inventarList[i].destroy();
        }
        inventarList = [];
    }

    //
    //
    //


    /**
     * storeView - Show the storeview
     *
     * @param  {type} list Current inventory items.
     */
    function storeView(list) {
        var i,
            currentItem,
            xPos = 210,
            amount;
        clearView();
        inventarItems = list;
        for (i = 0; i < plantsList.length; i++) {
            amount = getAmountValue(plantsList[i].id, list);
            currentItem = new InventoryView.StoreItems({
                gameEngine: gameEngine,
                type: plantsList[i].id,
                amount: amount,
                position: xPos
            });
            currentItem.init();
            currentItem.addEventListener("action", action);
            inventarList.push(currentItem);
            xPos += 90;
        }
    }


    /**
     * getAmountValue - Get the amount of a certain type.
     *
     * @param  {type} item Type of the searched item.
     * @param  {type} list List to be searched.
     */
    function getAmountValue(item, list) {
        var i, amount;
        for (i = 0; i < list.length; i++) {
            if (item === list[i].type) {
                return list[i].amount;
            } else {
                amount = 0;
            }
        }
        return amount;
    }


    /**
     * action - Handle action events from storeitems.
     *
     * @param  {type} event Incoming event.
     */
    function action(event) {
        switch (event.data.key) {
            case "minus":
                console.log("minus");
                that.notifyAll("onItemSellClicked", event.data.parent);

                break;
            case "plus":
                console.log("plus");
                that.notifyAll("onItemBuyClicked", event.data.parent);
            break;
        default:
            break;
        }
    }

    that.init = init;
    that.updateInventar = updateInventar;
    that.storeView = storeView;
    return that;
};
