
/**
 * GameEngineFactory - Factory for creating a new gameEngine.
 */
/*global Phaser*/
var Game = Game || {};
Game.GameEngineFactory = function(options) {
    var that = {},
        game,
        plantsList = options.plantsList,
        onFinished = options.onFinished;
    //
    //
    // INITIALIZERS

    /**
     * getGameEngine - Create a new game engine.
     *
     * @return {type}  The instance of the newly created game engine.
     */
    function getGameEngine() {
        game = new Phaser.Game(1280, 720, Phaser.AUTO, "gameCanvas", {
            preload: preload,
            create: create
        });

        return game;
    }
    /**
     * preload all assets
     */

    /**
     * preload - Preload all the graphic assets for the game.
     *
     * @return {type}  description
     */
    function preload() {
        var index = 0;
        game.load.image("background", "res/assets/canvasBackground_onProgress3.jpg");
        game.load.image("toolBox", "assets/toolBox.png");
        game.load.image("scoop", "assets/icon_scoop.png");
        game.load.image("wateringCan", "assets/icon_can.png");
        game.load.image("dead", "res/assets/dead.png");
        game.load.spritesheet("inventarBtn", "res/assets/menu.png");
        game.load.spritesheet("storeBtn", "res/assets/menu.png");
        game.load.spritesheet("plus", "res/assets/plus.png");
        game.load.spritesheet("minus", "res/assets/minus.png");

        for (index; index < plantsList.length; index++) {
            game.load.image(plantsList[index].id, "res/assets/" + plantsList[index].id + ".png");
            game.load.image(plantsList[index].id + "1", "res/assets/" + plantsList[index].id + "1.png");
            game.load.image(plantsList[index].id + "2", "res/assets/" + plantsList[index].id + "2.png");
            game.load.image(plantsList[index].id + "3", "res/assets/" + plantsList[index].id + "3.png");
        }
    }

    /**
     * create - Initialize game background.
     */
    function create() {
        game.add.sprite(0, 0, "background");
        onFinished();
    }

    //
    //
    //

    that.getGameEngine = getGameEngine;
    return that;
};
