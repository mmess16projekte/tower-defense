/**
 * GardenView - Shows the planted items of the game.
 */
/*global Game, GardenView, GartenAppUtils*/
var Game = Game || {};
Game.GardenView = function(options) {
    var that = new GartenAppUtils.EventPublisher,
        gameEngine,
        plantDictionary,
        plantedItems = [],
        maxLifePoints;


    /**
     * init - Initialize the GardenView.
     */
    function init() {
        gameEngine = options.gameEngine;
        plantDictionary = options.plantDictionary;
        maxLifePoints = options.maxLifePoints;
    }

    //
    //
    //

    /**
     * updatePlantedItems - Redraw planted items depending on input data.
     *
     * @param  {type} data New plant data.
     */
    function updatePlantedItems(data) {
        var i = 0,
            growthIndex,
            currentData,
            currentItem;

        clearPlantedItems();
        for (i; i < data.length; i++) {
            currentData = data[i];
            if (currentData.type != "") {
                growthIndex = currentData.growthPoints / plantDictionary.getPlantTypeData(currentData.type).maxGrowthPoints;
                currentItem = new GardenView.PlantedGardenItem({
                    gameEngine: gameEngine,
                    type: currentData.type,
                    growthIndex: growthIndex,
                    position: currentData.position,
                    isAlive: currentData.isAlive,
                    lifeIndex: currentData.lifePoints / maxLifePoints
                });
                currentItem.init();
                plantedItems.push(currentItem);
            }
        }
    }


    /**
     * clearPlantedItems - Remove all planted items.
     */
    function clearPlantedItems() {
        var i = 0;
        for (i; i < plantedItems.length; i++) {
            plantedItems[i].destroy();
        }
        plantedItems = [];
    }

    //
    //
    //

    that.updatePlantedItems = updatePlantedItems;
    that.clearPlantedItems = clearPlantedItems;
    that.init = init;

    return that;
};
