/**
 * InventoryModel - Data Model for all inventory items.
 */
/* global Game */
var Game = Game || {};
Game.InventoryModel = function(options) {
    var that = {},
        inventoryItems = [];


    /**
     * init - Initialize the inventory model.
     */
    function init() {
    }


    /**
     * setInventoryItems - Update inventory items.
     *
     * @param  {type} items New data.
     */
    function setInventoryItems(items){
        inventoryItems = items;
    }


    /**
     * indexOfInventoryItem - Get the index of a certain inventory item.
     *
     * @param  {type} type Type of the inventory item.
     * @return {type}      Index of the inventory item.
     */
    function indexOfInventoryItem(type) {
        var i = 0,
            currentItem;
        for (i; i < inventoryItems.length; i++) {
            currentItem = inventoryItems[i];
            if (currentItem.type == type) {
                return i;
            }
        }
        return -1;
    }

    /**
     * getInventoryItems - Get a list with all inventory items.
     */
    function getInventoryItems() {
        return inventoryItems;
    }



    /**
     * decreaseInventoryItemAmount - Decrease inventory item amount by 1.
     *
     * @param  {type} type Type of the items, where the amount has to be decreased.
     */
    function decreaseInventoryItemAmount(type) {
        var currentItem,
            amount;
        currentItem = inventoryItems[indexOfInventoryItem(type)];
        amount = currentItem.amount;
        if (amount > 1) {
            currentItem.amount = amount -= 1;
        } else {
            removeInventoryItem(type);
        }
    }

    /**
     * increaseInventoryItemAmount - Increase inventory item amount by 1.
     *
     * @param  {type} type Type of the items, where the amount has to be increased.
     */
    function increaseInventoryItemAmount(type) {
        var currentItem;
        if (hasInventoryItemOfType(type)) {
            currentItem = inventoryItems[indexOfInventoryItem(type)];
            currentItem.amount += 1;
        } else {
            inventoryItems.push({
                type: type,
                amount: 1
            });
        }
    }


    /**
     * getItenAmount - Get the amount of a certain item.
     *
     * @param  {type} type Type of the item.
     * @return {type}      Amount of the item.
     */
    function getItenAmount(type) {
        if (inventoryItems[indexOfInventoryItem(type)] != undefined) {
            return inventoryItems[indexOfInventoryItem(type)].amount;
        } else {
            return 0;
        }
    }


    /**
     * removeInventoryItem - Remove an item from the inventory.
     *
     * @param  {type} type Type of the item, that has to be removed.
     */
    function removeInventoryItem(type) {
        inventoryItems.splice(indexOfInventoryItem(type), 1);
    }


    /**
     * hasInventoryItemOfType - Returns, whether there is an item of a certain type in the inventory.
     *
     * @param  {type} type Type to be checked.
     * @return {type}      Whether there is an item of this type in the inventory.
     */
    function hasInventoryItemOfType(type) {
        return (indexOfInventoryItem(type) >= 0);
    }

    //
    //
    // 

    that.setInventoryItems = setInventoryItems;
    that.getInventoryItems = getInventoryItems;
    that.decreaseInventoryItemAmount = decreaseInventoryItemAmount;
    that.increaseInventoryItemAmount = increaseInventoryItemAmount;
    that.hasInventoryItemOfType = hasInventoryItemOfType;
    that.getItenAmount = getItenAmount;
    that.init = init;
    return that;
};
