/**
 * Model for the planted items.
 */
/* global Game */
var Game = Game || {};
Game.GardenModel = function(options) {
    var that = {},
        maxLifePoints,
        // most important var - contains all items in the garden
        plantedItems = [],
        plantDictionary;


    /**
     * init - Initialize the Garden Model.
     */
    function init() {
        console.log(options);
        plantDictionary = options.plantDictionary;
        maxLifePoints = options.config.max_lifepoints;
    }

    //
    //
    // planted items
    //
    /**
     * setPlantedItems - Update the list of planted items.
     *
     * @param  {type} items New values
     */

    function setPlantedItems(items) {
        plantedItems = items;
    }


    /**
     * getPlantedItems - Get the list with planted items sorted by their position (good for rendering, so that there is no strange overlap).
     *
     * @return {type}  List with planted items
     */
    function getPlantedItems() {
        plantedItems.sort(function(a, b) {
            return parseInt(a.position) - parseFloat(b.position);
        });
        return plantedItems;
    }


    /**
     * addPlantAt - Set a new plant of a certain type to a position.
     *
     * @param  {type} position Position where the plant is to be set.
     * @param  {type} type     Type of the plant to be set.
     */
    function addPlantAt(position, type) {
        plantedItems.push({
            position: position,
            type: type,
            growthPoints: 0.0,
            lifePoints: maxLifePoints / 2,
            isAlive: true
        });
    }


    /**
     * removePlantAt - Remove a plant from the list of planted items.
     *
     * @param  {type} position Position of the plant to be removed.
     */
    function removePlantAt(position) {
        // remove plant from list
        plantedItems.splice(indexOfPlantAt(position), 1);
    }


    /**
     * waterPlantAt - Mark a plant as watered.
     *
     * @param  {type} position Position of the plant to be watered.
     */
    function waterPlantAt(position) {
        var selectedItem = plantedItems[indexOfPlantAt(position)];
        selectedItem.lifePoints = maxLifePoints;
    }


    /**
     * hasPlantAt - Check, whether there is a plant at a certain position.
     *
     * @param  {type} position Position to be checked.
     * @return {type}          Whether there is a plant on the position.
     */
    function hasPlantAt(position) {
        var indexOfPlant = indexOfPlantAt(position);
        if (indexOfPlant >= 0) {
            if (plantedItems[indexOfPlant].type != "") {
                return true;
            }
        }
        return false;
    }


    /**
     * indexOfPlantAt - Get the plantlist index of a plant at a certain position.
     *
     * @param  {type} position Position of the plant.
     * @return {type}          Index of the plant in plantlist. -1 if not found.
     */
    function indexOfPlantAt(position) {
        var i = 0,
            currentItem;
        for (i; i < plantedItems.length; i++) {
            currentItem = plantedItems[i];
            if (currentItem.position == position) {
                return i;
            }
        }
        return -1;
    }


    /**
     * getTypeOfPlantAt - Get the type of the plant at a certain position.
     *
     * @param  {type} position Position where the plant is located.
     * @return {type}          Type of the plant.
     */
    function getTypeOfPlantAt(position) {
        var data = plantedItems[indexOfPlantAt(position)];
        return data.type;
    }


    /**
     * isPlantAtReady - Check, whether the plant is ready to harvest.
     *
     * @param  {type} position Position of plant to be checked.
     * @return {type}          Whether the plant is fully grown.
     */
    function isPlantAtReady(position) {
        var data = plantedItems[indexOfPlantAt(position)];
        return (data.growthPoints >= plantDictionary.getPlantTypeData(data.type).maxGrowthPoints);
    }

    //
    //
    //

    that.setPlantedItems = setPlantedItems;
    that.getPlantedItems = getPlantedItems;
    that.addPlantAt = addPlantAt;
    that.removePlantAt = removePlantAt;
    that.waterPlantAt = waterPlantAt;
    that.hasPlantAt = hasPlantAt;
    that.indexOfPlantAt = indexOfPlantAt;
    that.isPlantAtReady = isPlantAtReady;
    that.getTypeOfPlantAt = getTypeOfPlantAt;
    that.init = init;
    return that;
};
