
/**
 * Main Application class for the game
 */
/* global GartenApp, $, APIKeys, google, GameConfig */
var GartenApp = (function() {
    var that = {},
        loginController,
        userService,
        placesService,
        game,
        gameService,
        registerController,
        forms,
        canvas,
        location,
        loadComplete,
        statusBar;

//
//
//

    /**
     * init - Initialize the application.
     */
    function init() {
        initLoginController();
        initRegisterController();
        initPlacesService();
        initUserService();
        initGame();
    }


    /**
     * initUserService - Initialize the user service and register callbacks.
     */
    function initUserService() {
        userService = new GartenApp.UserService({});
        userService.addEventListener("onLoginSuccessful", onLoginSuccessful);
        userService.addEventListener("onLoginFailed", onLoginFailed);
        userService.addEventListener("onRegisterSuccessful", onRegisterSuccessful);
        userService.addEventListener("onUserNameChecked", onUserNameChecked);
    }


    /**
     * initLoginController - Initialize login controller and register callbacks.
     */
    function initLoginController() {
        loginController = new GartenApp.LoginController({
            loginForm: $("#loginForm"),
            username: $("#nickNameInputLogin"),
            password: $("#passWordInputLogin")
        });
        loginController.init();
        loginController.addEventListener("loginSubmit", onLoginSubmit);
    }


    /**
     * initRegisterController - Initialize Register Controller and register callbacks.
     */
    function initRegisterController() {
        registerController = new GartenApp.RegisterController({
            inputNickName: $("#nickNameInputRegister"),
            inputLocationName: $("#locationNameInputRegister"),
            signupForm: $("#signUpForm")
        });
        registerController.init();
        registerController.addEventListener("registerUsernameEntered", onRegisterUsernameEntered);
        registerController.addEventListener("onRegisterSubmit", onRegisterSubmit);

    }


    /**
     * initPlacesService - Initialize the places service for cities autocomplete.
     */
    function initPlacesService() {
        placesService = new GartenApp.PlacesService({
            placesApiKey: APIKeys.getGooglePlacesKey(),
            bodyElement: document.getElementsByTagName("body")[0]
        });
        placesService.init();
    }


    /**
     * initGame - Initialize the game module and related services and set callbacks.
     */
    function initGame() {
        game = new GartenApp.Game({
            config: GameConfig.get()
        });
        gameService = new GartenApp.GameService();
        userService.init();
        game.addEventListener("onAction", onPlayerAction);
        game.addEventListener("onLogout", onLogout);
        game.addEventListener("onLoadComplete", onLoadComplete);
        gameService.addEventListener("onTick", onTick);
        gameService.addEventListener("onUserInformation", onUserInformation);
    }

    //
    //
    // CALLBACKS


    /**
     * onLogout - Callback when the logout-button is clicked.
     */
    function onLogout() {
        changeViewOnLogout();
        userService.logout();
        location.reload();
    }


    /**
     * onLoadComplete - Called, when the gameEngine has loaded all assets and is fully initialized.
     */
    function onLoadComplete() {
        loadComplete = true;
        gameService.requestUpdate();
    }


    /**
     * onLoginFailed - Called, when the server gives feedback, that the credentials were incorrect.
     */
    function onLoginFailed() {
        loginController.showInvalidLogin();

    }


    /**
     * onLoginSuccessful - Called, when the server gives feedback, that the credentials were correct.
     *
     * @param  {type} event Config data for gameservice init.
     */
    function onLoginSuccessful(event) {
        // start Game
        // We have to wait, because if the user is logged in already,
        // the Game can not be initialized correctly
        changeViewOnLogin();
        gameService.init(event.data);
    }

    /**
     * onUserInformation - Called, when game starts, sends specified location
     */
    function onUserInformation(event){
        location = event.data.city;
    }

    /**
     * onRegisterSuccessful - Called, when the server gives feedback, that the registration has been completed.
     *
     * @param  {type} event Data for loginSubmit.
     */
    function onRegisterSuccessful(event) {
        onLoginSubmit({
            data: {
                passWord: event.data.password,
                nickName: event.data.name
            }
        });
    }


    /**
     * onTick - Called, when the server updates or has been requested information update.
     *
     * @param  {type} event All player, inventory and garden information necessary to update the game views.
     */
    function onTick(event) {
        var time = new Date();

        if(loadComplete){
            game.updateStatusData({
                weather: event.data.weather,
                temperature: event.data.temperature,
                cash: event.data.cash,
                position: location,
                time: time
            });
            game.updateInventory({
                inventoryItems: event.data.inventory
            });
            game.updatePlantedItems({
                plantedItems: event.data.plants
            });
        }

    }


    /**
     * onRegisterSubmit - Called, when the registerController sends valid information to be sent to the server.
     *
     * @param  {type} data User data to be submitted.
     */
    function onRegisterSubmit(data) {
        userService.register(data);
    }


    /**
     * onRegisterUsernameEntered - Called, when the RegisterController's username field is changed- fires a request, if the name is already taken.
     *
     * @param  {type} event Username data.
     */
    function onRegisterUsernameEntered(event) {
        var username = event.data;
        userService.isUserNameAvailable(username);
    }


    /**
     * onLoginSubmit - Called, when the loginController submits user data for login.
     *
     * @param  {type} event User Credentials.
     */
    function onLoginSubmit(event) {
        var passWord = event.data.passWord,
            nickName = event.data.nickName;
        userService.login(nickName, passWord);
    }


    /**
     * onPlacesApiLoaded - Called, when the google places api has been initialized correctly.
     */
    function onPlacesApiLoaded() {
        var autoComplete = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */
            (document.getElementById("locationNameInputRegister")), {
                types: ["geocode"]
            });
        registerController.setAutoComplete(autoComplete);
    }

    /**
     * onPlayerAction - Called when the game module recognizes a user action.
     *
     * @param  {type} action Necessary information about the action.
     */
    function onPlayerAction(action) {
        gameService.send(action);
    }

    /**
     * onUserNameChecked - Called, when the server gives feedback, whether a name is taken.
     *
     * @param  {type} event Information about validity of the desired username.
     */
    function onUserNameChecked(event) {
        registerController.setUsernameValid(event.data);
    }

    //
    //
    //

    /**
     * changeViewOnLogin - Init and show the gameView after successful login.
     */
    function changeViewOnLogin() {
        forms = document.getElementById("forms");
        forms.setAttribute("hidden", true);
        canvas = document.getElementById("gameCanvas");
        canvas.removeAttribute("hidden");
        statusBar = document.getElementById("status");
        statusBar.removeAttribute("hidden");
        game.init();
    }


    /**
     * changeViewOnLogout - Show the login view and hide the game on Logout.
     */
    function changeViewOnLogout() {
        canvas.setAttribute("hidden", true);
        statusBar.setAttribute("hidden", true);
        forms.removeAttribute("hidden");
    }

    //
    //
    //

    that.init = init;
    that.onPlacesApiLoaded = onPlacesApiLoaded;
    return that;
}());
