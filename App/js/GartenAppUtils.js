/**
* DESCRIPTION: This utility class contains functions for project-wide use, e.g. the Event Publisher.
*/


window.GartenAppUtils = window.GartenAppUtils || (function() {
    "use strict";
    var that = {},
        EventPublisher;


    /**
     * EventPublisher - Prototype for EventPublisher.
     */
    EventPublisher = function() {
        this.listeners = {};
    };

    /**
     *  addEventListener - adds a listener for a certain event to the EvenPublisher
     */
    EventPublisher.prototype.addEventListener = function(event,
    listener) {
        if (this.listeners[event] === undefined) {
            this.listeners[event] = [];
        }
        this.listeners[event].push(listener);
    };

    /**
     *  addEventListener - removes a listener for a certain event to the EvenPublisher
     */
    EventPublisher.prototype.removeEventListener = function(event,
    listener) {
        var index;
        if (this.listeners[event] === undefined) {
            return;
        }
        index = this.listeners[event].indexOf(listener);
        if (index > -1) {
            this.listeners[event].splice(index, 1);
        }
    };

    /**
     *  notifyAll - calls back all the listeners, that have been registered for a certain event.
     */
    EventPublisher.prototype.notifyAll = function(event, data) {
        var i;
        for (i = 0; i < this.listeners[event].length; i++) {
            this.listeners[event][i]({
                target: this,
                data: data
            });
        }
    };


    that.EventPublisher = EventPublisher;
    return that;
}());
