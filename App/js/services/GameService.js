/* global GartenApp, GartenAppUtils, io */
GartenApp.GameService = function() {
    var that = new GartenAppUtils.EventPublisher,
        socket;

    // gameService wrappes the socket io object and pushes events
    // from the server into the gardening app and sends events
    function init(config) {
        socket = io.connect("http://localhost:3000");
        socket.on("tick", onTick);
        socket.on("weather", onWeatherUpdated);
        socket.on("info", onUserInformation);

        // Authorize client with token
        socket.emit("authorize", config.token);
    }



    // Sends basic user information and location information
    function onUserInformation(data) {
        that.notifyAll("onUserInformation", data);
    }

    // Listens for game field updates (typically every 10sec)
    function onTick(event) {
        that.notifyAll("onTick", event);
    }

    // Receives and listens for weather update events
    function onWeatherUpdated(weather) {
        that.notifyAll("onWeatherUpdated", weather);
    }

    // Uniform send method, sends an action object from the to the server
    // to avoid multiple callbacks for each action
    function send(action) {
        socket.emit("action", action);
    }

    // Request an game field update when game is loaded
    function requestUpdate() {
        socket.emit("update");
    }

    that.requestUpdate = requestUpdate;
    that.send = send;
    that.init = init;
    return that;
};
