/**
* PlacesService - This service is an interface to the google places api, which delivers geodata for selected cities.
*/
/* global GartenApp, GartenAppUtils */
var GartenApp = GartenApp || {};
GartenApp.PlacesService = function(options){
    var that = new GartenAppUtils.EventPublisher,
        apiKey;


    /**
     * init - Initialize the service.
     */
    function init(){
        apiKey = options.placesApiKey;
        initPlacesApi(options.bodyElement);
    }

    /**
     * initPlacesApi - Initialize the places API.
     *
     * @param  {type} bodyElement HTML body, needed for displaying the autocomplete dropdown.
     */
    function initPlacesApi(bodyElement){
        var src = "https://maps.googleapis.com/maps/api/js?key="+apiKey+"&signed_in=true&libraries=places&callback=GartenApp.onPlacesApiLoaded",
            scriptRef;
        scriptRef = document.createElement("script");
        scriptRef.setAttribute("type","text/javascript");
        scriptRef.setAttribute("src", src);
        scriptRef.async = true;
        scriptRef.defer = true;
        bodyElement.appendChild(scriptRef);
    }

    //
    //
    //
    
    that.init = init;
    return that;
};
