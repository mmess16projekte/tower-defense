/*
  onLoginFailed
  onLoginSuccessful
  onRegisterSuccessful
*/

/* global GartenApp, GartenAppUtils, $, Cookies */
var GartenApp = GartenApp || {};
GartenApp.UserService = function(){
    var that = new GartenAppUtils.EventPublisher,
        baseUrl = "http://localhost:3000",
        token;

    function init(){
        // When user was already registered, an cookie with token must be
        // saved in browser
        if(getCookie("token")) {
            // Cookie available -> go to gameView
            token = getCookie("token");
            that.notifyAll("onLoginSuccessful", {
                "token": token
            });

        } else {
            // Login required
        }
    }

    // Login the user and send a request with credentials to /authenticate
    function login(nickName, passWord){
      // TODO: handle login procedure here!
        var loginConfig = {
            url: baseUrl + "/authenticate",
            type: "POST",
            headers: {
                Authorization: "Basic " + btoa(nickName + ":" + passWord)
            },
            error: function() {
                that.notifyAll("onLoginFailed", {});
            }
        };
        $.ajax(loginConfig).done(function(data) {
            //parseToken(data.token);
            if(data.token == null) {
                that.notifyAll("onLoginFailed");
            } else {
                setCookie(data.token);
                that.notifyAll("onLoginSuccessful", {token: data.token});
            }

        });
    }

    // Logout user by removing token
    function logout() {
        Cookies.remove("token");
    }

    // Setting the cookie
    function setCookie(token) {
        Cookies.set("token", token);
    }

    // Receive the cookie
    function getCookie() {
        return Cookies.get("token");
    }

    // Send an POST request to /user with user information
    function register(input){
      // TODO: handle register
        var data = input.data;

        var userData = {
            name: data.username,
            mail: data.email,
            password: data.password,
            location: {
                lat: data.location.latitude,
                lon: data.location.longitude
            }
        };

        var registerConfig = {
            url: baseUrl + "/user",
            type: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            data: JSON.stringify(userData),
            error: function() {
                // Catch error
            }
        };



        $.ajax(registerConfig).done(function() {
            that.notifyAll("onRegisterSuccessful", userData);
        });
    }


    // Checks if a user already exists
    function isUserNameAvailable(userId) {
        var nameAvailableConfig = {
            url: baseUrl + "/user/" + userId,
            type: "GET",
            headers: {
                "Content-Type": "application/json"
            },
            error: function() {
                // Catch error
            }
        };



        $.ajax(nameAvailableConfig).done(function(data) {
            if($.isEmptyObject(data.error)) {
                that.notifyAll("onUserNameChecked", true);
            } else {
                that.notifyAll("onUserNameChecked", false);
            }
        });
    }

    that.init = init;
    that.login = login;
    that.logout = logout;
    that.register = register;
    that.isUserNameAvailable = isUserNameAvailable;
    return that;
};
