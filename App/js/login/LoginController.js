/*
  LoginController: A controller, that handles input for the login procedure.
*/

/* global GartenApp, GartenAppUtils */
var GartenApp = GartenApp || {};
GartenApp.LoginController = function(options) {
    var that = new GartenAppUtils.EventPublisher,
        loginForm,
        username,
        password,
        loginValidator;

//
//
// INITIALIZERS

    /**
     * init - Initialize the module.
     */
    function init() {
        loginForm = options.loginForm;
        username = options.username;
        password = options.password;
        initLoginValidator();
        loginForm[0].onsubmit = onLoginSubmit;
    }


    /**
     * initLoginValidator - Set validation rules for username and password.
     */
    function initLoginValidator() {
        loginForm.bootstrapValidator({
            container: "#messages",
            fields: {
                usernameLogin: {
                    validators: {
                        notEmpty: {
                            message: "Username or password is incorrect and can\'t be empty "
                        }
                    }
                },
                passwordLogin: {
                    validators: {
                        notEmpty: {}
                    }
                }
            }
        });
        loginValidator = loginForm.data("bootstrapValidator");
    }

    //
    //
    // CALLBACKS

    /**
     * onLoginSubmit - Called, when the form is submitted. The submit will only be passed, if the form input is valid.
     *
     * @param  {type} event If
     * @return {type}       description
     */
    function onLoginSubmit(event) {
        var loginEvent = {
            nickName: loginValidator.getFieldElements("usernameLogin").val(),
            passWord: loginValidator.getFieldElements("passwordLogin").val()
        };
        if (loginValidator.isValid()) {
            // NOTE: removing the onsubmit might seem like a hacky workaround for the form submitted twice,
            // but the validator would send several submits despite of preventDefault.
            loginForm[0].onsubmit = undefined;
            that.notifyAll("loginSubmit", loginEvent);
        }

        event.preventDefault();
        return false;
    }

    //
    //
    //

    /**
     * showInvalidLogin - Removes Form input on login feedbackIcons and sets input fields invalid.
     *
     * @return {type}  description
     */
    function showInvalidLogin() {
        // TOOD: not shown yet
        loginForm.bootstrapValidator("resetForm", true);
        loginForm.bootstrapValidator("updateStatus",username, "INVALID");
        loginForm.bootstrapValidator("updateStatus", password, "INVALID");
        // NOTE: reactivate submit if credentials are correct
        loginForm[0].onsubmit = onLoginSubmit;
    }

    //
    //
    //

    that.init = init;
    that.showInvalidLogin = showInvalidLogin;
    return that;
};
