var routing = require("./modules/Routing.js");
var database = require("./modules/Database.js");
var game = require("./modules/GameRouting.js");
var config = require("./Config.json");
var server;

database.init(config);


// When database connection is successful, start up our api service
database.on("connectionSuccess", function() {

    routing.init({
        port: 3000,
        appSecret: config.appSecret,
        connection: database
    });
    server = routing.run();
    game.init(server, database);
});
database.connect();
