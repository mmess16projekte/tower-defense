var config = require("../Config.json");
var Ticker = require("./GameTicker.js");
var security = require("./SecurityHelper.js");
var weatherProvider = require("./WeatherProvider.js");
var EventEmitter = require("events").EventEmitter;
var gameEvents = new EventEmitter();
var PlantProvider = require("./PlantProvider.js");
var async = require("async");


module.exports = (function() {
    var that = {},
        io,
        db = require("./Database.js"),
        ticker,
        weatherTicker;


    function init(server, database) {
        db = database;

        // Check weather on startup
        updateWeather();

        // Init game ticker (let our plants grow)
        ticker = new Ticker();
        ticker.init(config.gardenUpdate);

        // Init weatherUpdate ticker (updates weather, which is much more rare)
        weatherTicker = new Ticker();
        weatherTicker.init(config.weatherUpdate);

        weatherTicker.on("tick", function() {
            updateWeather();
        });

        ticker.on("tick", function() {
            updateAllUserPlants();
        });


        // Manage in and outgoing events
        io = require("socket.io")(server, {log: true});
        io.on("connection", function(socket) {
            socket.name = null;

            updateWeather();

            gameEvents.on("onWeatherUpdated", function() {
                if(socket.name != null) {

                    db.getGameByUser(socket.name).then(function(game) {
                        var weatherPackage = {
                            weather: game.weather,
                            temperature: game.temperature
                        };
                        socket.emit("weather", weatherPackage);
                    }, function() {
                        // Error callback
                    });
                }
            });

            ticker.on("tick", function() {

                db.getGameByUser(socket.name).then(function(game) {
                    socket.emit("tick", game);
                });
                //socket.emit("tick", {name: socket.name});
            });

            socket.on("action", function(action) {
                // Here we get all of our actions performed by the player
                // after successful login

                if(action.data.action == "PLANT") {
                    actionPlant(socket, action);
                } else if(action.data.action == "WATER") {
                    actionWater(socket, action);
                } else if(action.data.action == "REMOVE") {
                    actionRemove(socket, action);
                } else if(action.data.action == "SELL") {
                    actionSell(socket, action);
                } else if(action.data.action == "BUY") {
                    actionBuy(socket, action);
                } else if(action.data.action == "HARVEST") {
                    actionHarvest(socket, action);
                }
            });

            // Check if submitted token is valid
            socket.on("authorize", function(token) {
                var checkedToken = security.verifyToken(token);
                socket.emit("auth", checkedToken);
                if(!checkedToken.isValid) {
                    socket.disconnect();
                }

                // User is authenticated successfully
                socket.name = checkedToken.user;

            });

            socket.on("update", function() {
                sendUpdate(socket);
                sendLocation(socket);
            });
        });
    }

    function actionPlant(socket, action) {
        db.plantItem(socket.name, {
            type: action.data.type,
            location: action.data.location,
            lifePoints: 100
        }).then(function() {
            db.updateInventory(socket.name, {
                type: action.data.type,
                amount: -1
            }).then(function() {
                sendUpdate(socket);
            });
        }, function() {
            // Error callback
        });

    }

    // Perform action to buy a plant (from store to inventory)
    function actionBuy(socket, action) {
        var plant = PlantProvider.getPlantProperties(action.data.type);
        db.getGameByUser(socket.name).then(function(game) {
            // Do we have enough money to buy this lovely plant?
            if(game.cash-plant.buy >= 0) {
                // yes!

                db.updateCash(socket.name, -plant.buy).then(function() {
                    db.updateInventory(socket.name,
                        {type: plant.type, amount: 1}
                    ).then(function(){
                        sendUpdate(socket);
                    });
                });
            }
        });
    }

    // Perform action to sell a plant from inventory
    function actionSell(socket, action) {
        // Decrease inventory by selled item
        var plant = PlantProvider.getPlantProperties(action.data.type);
        db.updateInventory(socket.name, {
            type: action.data.type,
            amount: -1
        }).then(function() {
            db.updateCash(socket.name, plant.sell).then(function() {
                sendUpdate(socket);
            });
        });
    }

    // Perform action to water a plant
    function actionWater(socket, action) {
        db.waterItem(socket.name, {
            location: action.data.location
        }).then(function() {
            sendUpdate(socket);
        });
    }

    // Perform action to remove a (died) plant
    function actionRemove(socket, action) {
        // When removing an fully growth item, we need to add it
        // to our inventory and then delete it
        db.removeItem(socket.name, {
            location: action.data.location
        }).then(function() {
            sendUpdate(socket);
        });
    }

    // Perform action to harvest a plant
    function actionHarvest(socket, action) {
        var amount,
            benefitIndex = PlantProvider.getPlantProperties(action.data.type).benefitIndex;

        amount = Math.floor((Math.random()*3)+1) * benefitIndex;

        db.updateInventory(socket.name, {
            type: action.data.type,
            amount: amount
        }).then(function(err) {
            if(err) {
                // Error callback
            }
            db.removeItem(socket.name, {
                location: action.data.location
            }).then(function() {
                sendUpdate(socket);
            });
        });


    }

    // After each action, request or weatherUpdate we send the user
    // the updated game field
    function sendUpdate(socket) {
        db.getGameByUser(socket.name).then(function(game) {
            socket.emit("tick", game);
        });
    }

    function sendLocation(socket) {
        db.getUser(socket.name).then(function(user) {
            socket.emit("info", user);
        });
    }

    function updateAllUserPlants() {
        db.getAllUsers().then(function(users) {
            async.eachLimit(users, 1,function(user,callback){
                updatePlantsByUser(user).then(function() {
                    callback();
                }, function(err) {
                    callback(err);
                });
            }, function() {
                // Error callback if anything bad happens
            });
        });
    }

    // Updates all plants owned by a user
    function updatePlantsByUser(user) {
        // First get players game from the database and iterate through all plant
        // field
        return new Promise(function(resolve) {
            var plants;
            db.getGameByUser(user).then(function(game) {
                plants = game.plants.map(function(o) {
                    // Nothing to do here. The poor plant is dead
                    if(o == null || !o.isAlive) {
                        return o;
                    }

                    // Let the plant die if lifePoints are lower than 1
                    if(o.lifePoints <= 0) {
                        o.isAlive = false;
                        return o;
                    }

                    // Water each plant on your game field
                    if(game.weather == "sunny" || game.weather == "cloudy") {
                        if(o.lifePoints > 0) {
                            o.lifePoints -= 1;

                        }
                    } else if(game.weather == "rainy" || game.weather == "stormy" || game.weather == "snowy") {
                        if(o.lifePoints < 100) {
                            o.lifePoints += 1;
                        }
                    } else {
                        if(o.lifePoints <= 100 && o.lifePoints > 0) {
                            // Do nothing
                        }
                    }

                    if(o.isAlive) {
                        o.growthPoints = calculateGrowthpoints({
                            currentPoints: o.growthPoints,
                            weather: game.weather,
                            temperature: game.temperature,
                            type: o.type
                        });
                    }
                    return o;
                });
                // Now we can save all plants to the database
                db.updateAllPlants(user, plants).then(function() {
                    resolve();
                }, function() {
                    // Error callback
                });
            }, function() {
                // Error callback
            });
        });
    }

    function calculateGrowthpoints(params) {
        var points = params.currentPoints,
            weatherFactor = 0,
            heatFactor = 0,
            maxGrowthPoints = PlantProvider.getPlantProperties(params.type).maxGrowth;

        if(points == maxGrowthPoints) {
            return maxGrowthPoints;
        }

        if(params.weather == "sunny") {
            weatherFactor = 3;
        } else if(params.weather == "cloudy") {
            weatherFactor = 2;
        } else if(params.weather == "rainy") {
            weatherFactor = 1;
        } else {
            weatherFactor = 0;
        }

        if(params.temperature > 0) {
            heatFactor = Math.floor(params.temperature/10);
        } else {
            heatFactor = 0;
        }

        points += weatherFactor * heatFactor;

        if(points > maxGrowthPoints) {
            points = maxGrowthPoints;
        }
        return points;
    }

    function updateWeather() {
        db.getAllUsers().then(function(users) {
            async.eachLimit(users, 1,function(user,callback){
                weatherProvider.getWeatherForId(user.cityId).then(function(data) {
                    db.updateWeather(user._id, data).then(function() {
                        callback();
                    }, function(err) {
                        callback(err);
                    });

                }, function(err) {
                    callback(err);
                });
            }, function(err) {
                if(err) {
                    // Something bad happens here
                } else {
                    gameEvents.emit("onWeatherUpdated");
                }
            });
        });
    }

    that.init = init;
    return that;
})();
