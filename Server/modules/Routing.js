var express = require("express");
var cors = require("cors");
var bodyParser = require("body-parser");
var security = require("./SecurityHelper");
var weatherProvider = require("./WeatherProvider.js");
var async = require("async");

module.exports = (function() {
    var that = {},
        app,
        db;

    function init(config) {
        that.port = config.port;
        db = config.connection;
        security.init(config.appSecret);

        // Create app
        app = express();

        // Serve game from uri
        app.use("/game",express.static("../App"));

        // Use middlewares
        // Enable cors for avoiding cross origin failures
        app.use(cors());

        // Set Content-Type of each request to json for our api
        app.use(function(req,res,next) {
            res.setHeader("Content-Type", "application/json");
            next();
        });

        // Use an bodyParser to get json objects out of each request
        app.use(bodyParser.json());


        // Init routes
        initRoutes();
    }

    function initRoutes() {
        // Create new user
        app.post("/user", createUser);

        // Authenticate user
        app.post("/authenticate", authenticateUser);

        // Show user
        app.get("/user/:id", showUser);

        // Validate
        app.get("/validate", validation);
    }

    function createUser(req, res) {
        var user, location = {};
        user = req.body;
        location.lat = user.location.lat;
        location.lon = user.location.lon;
        user.password = security.createHash(user.password);

        // Async waterfall to create a user in the database
        async.waterfall([
            function(callback) {
                // Get weather and the Id of the city which is required to
                // query the OpenWeatherMap
                weatherProvider.getWeatherFor(location).then(function(weather) {
                    callback(null,weather);
                });
            },
            function(weather, callback) {
                // Write user to user collection
                user.city = weather.city;
                user.cityId = weather.id;
                db.createUser(user).then(function(data) {
                    callback(null, data);
                }, function(err) {
                    callback(err);
                });
            },
            function(data, callback) {
                // Create an empty game field in game collection
                db.createGame(user._id).then(function() {
                    callback(null, data);
                }, function(err) {
                    callback(err);
                });
            },
            function(data, callback) {
                // Create empty plants on your field
                db.createPlants(user._id).then(function(data) {
                    callback(null, data);
                }, function(err) {
                    callback(err);
                });
            },
            function(data, callback) {
                // Create empty (with initial values) inventory
                db.createInventory(user._id).then(function(data) {
                    callback(null,data);
                }, function(err) {
                    callback(err);
                });
            }],
            function(err) {
                // And finally respond to the client with according message
                var result;
                if(err) {
                    // If there was an error, send an 500 internal server error
                    // with debug message
                    result = {status: "Error creating user", msg: err.errmsg};
                    res.statusCode = 500;
                    res.json(result);
                } else {
                    // Else say ok (201)
                    result = {status: "User created"};
                    res.statusCode = 201;
                    res.json(result);
                }
            });


    }

    // Authenticates a user and sends the token
    function authenticateUser(req, res) {
        // Validate credentials
        // Create token
        var response = {};
        var credentials = security.decodeBasicAuth(req.headers.authorization);

        // Check if we have meaningful information given
        if(credentials == null) {
            // Invalid credentials
            res.statusCode = 401;
            response.error = "No valid header. Must be base64 encoded.";
            res.json(response);
        }

        // Query the database if the hashed password is correct
        db.checkCredentials(credentials.user, security.createHash(credentials.password))
            .then(function(cred) {
                response = {};
                if(cred.valid) {
                    response.token = security.createToken(credentials.user);
                    res.json(response);
                } else {
                    response.error = "Invalid credentials";
                    res.statusCode = 401;
                    res.json(response);
                }
            }, function(err) {
                res.json({error: err});
            });
    }

    // Validates a token
    function validation(req,res) {
        var token = req.headers.authorization, valid = false;
        valid = security.verifyToken(security.decodeBearerAuth(token));
        res.json({"Token": valid});
    }

    // Route to show user information
    function showUser(req, res) {
        db.getUser(req.params.id).then(function(user) {
            if(user == null) {
                res.json({error: "User not found"});
            } else {
                res.json(user);
            }
        }, function(err) {
            res.json({error: err});
        });


    }

    // Start the server
    function run() {
        return app.listen(that.port);
    }

    that.run = run;
    that.init = init;
    return that;
}());
