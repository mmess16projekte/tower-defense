var config = require("../Config.json");
var request = require("request");

module.exports = (function() {
    var that = {},
        baseUri = "http://api.openweathermap.org/data/2.5/weather?";

    // Get weather for location (Latitude, Longitude required)
    // Most important for this function is to initialize user and
    // fetch city id
    function getWeatherFor(location) {
        var uri = baseUri + "lat=" + location.lat + "&lon=" + location.lon
                    + "&APPID=" + config.weatherApiKey,
            result = {},
            response;
        return new Promise(function(resolve, reject) {
            request(uri, function(err, res, body) {
                if(err) {
                    reject(err);
                }
                if(res.statusCode != 200) {
                    reject();
                }
                response = JSON.parse(body);
                result.weather = mapWeather(response.weather[0].id);
                result.id = response.id;
                result.city = response.name;
                result.temperature = response.main.temp;
                resolve(result);
            });
        });
    }

    // Get weather when we already know the city id
    function getWeatherForId(cityId) {
        var uri = baseUri + "id=" + cityId + "&APPID=" + config.weatherApiKey + "&units=metric",
            result = {},
            response,
            options = {
                url: uri,
                headers: {
                    "Accept": "application/json"
                }
            };
        return new Promise(function(resolve, reject) {
            request(options, function(err, res, body) {
                if(err) {
                    reject(err);
                }
                if(res.statusCode != 200) {
                    reject();
                }
                try {
                    response = JSON.parse(body);
                    result.weather = mapWeather(response.weather[0].id);
                    result.id = response.id;
                    result.temperature = response.main.temp;
                    resolve(result);
                } catch(err) {
                    reject();
                }

            });
        });
    }

    // Mapping WeatherMaps API codes to our needs in the game
    function mapWeather(weatherId) {
        var id;
        // Category 800 is defined as "sunny"
        if(weatherId == 800) {
            return "sunny";
        }

        // We only need the first number to categorize weather
        id = Math.floor(weatherId/100);

        // Category 3xx and 5xx is defined as "rainy"
        if(id == 3 || id == 5) {
            // Its raining
            return "rainy";
        }
        // 8xx is defined as cloudy
        else if(id == 8) {
            return "cloudy";
        }
        // 6xx is defined as snowy
        else if(id == 6) {
            return "snowy";
        }

        else if(id == 2 || 9) {
            return "stormy";
        }

        // If we dont exactly know the id (e.g. hurricane, volcano ash etc),
        // we go into default case "rainy"
        return "rainy";
    }

    that.getWeatherFor = getWeatherFor;
    that.getWeatherForId = getWeatherForId;
    return that;
}());
