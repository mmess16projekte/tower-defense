var mongoose = require("mongoose");
var EventEmitter = require("events").EventEmitter;
var databaseEvents = new EventEmitter();
var PlantProvider = require("./PlantProvider.js");

// Import schemas
var userSchema = require("./DbModels/UserSchema.js");
var gameSchema = require("./DbModels/GameSchema.js");

module.exports = (function() {
    var that = databaseEvents,
        db = mongoose.connection,
        mongoUri,
        User,
        Game;


    function init(dbConf) {
        // Connect to MongoLab
        mongoUri = "mongodb://"+ dbConf.user + ":"+ dbConf.password +"@"
                + dbConf.host + ":" + dbConf.port + "/" + dbConf.database;
        db.once("open", connectionSuccess);

        // Inititalize mongoose schemas
        initSchemas();
    }

    function initSchemas() {
        User = mongoose.model("User", userSchema);
        Game = mongoose.model("Game", gameSchema);
    }

    function createUser(data) {
        var user;
        return new Promise(function(resolve, reject) {
            data._id = data.name;
            data.name = null;
            user = new User(data);
            user.save(function(err) {
                if(err) {
                    reject(err);
                } else {
                    resolve();
                }
            });
        });
    }

    function getUser(user) {
        return new Promise(function(resolve, reject) {
            User.findOne({_id: user},{__v: 0,password: 0, location: 0}, function(err, user) {
                if(err) {
                    reject(err);
                } else {
                    resolve(user);
                }
            });
        });
    }

    function createGame(user) {
        return new Promise(function(resolve, reject) {
            var game = new Game({
                "_id": user
            });
            game.save(function(err) {
                if(err) {
                    reject(err);
                } else {
                    resolve();
                }
            });
        });
    }

    function getAllUsers() {
        return new Promise(function(resolve, reject) {
            User.find({},{_id:1, cityId : 1}, function(err, users) {
                if(err) {
                    reject(err);
                } else {
                    resolve(users);
                }
            });
        });
    }

    function checkCredentials(userName, hashedPassword) {
        return new Promise(function(resolve, reject) {
            User.findOne({_id: userName}, function(err, res) {
                if(err) {
                    reject(err);
                }
                if(res != null){
                    if(hashedPassword == res.password) {
                        resolve({valid: true});
                    } else {
                        resolve({valid: false});
                    }
                }
                reject();
            });
        });
    }

    function updateWeather(user, data) {
        return new Promise(function(resolve, reject) {
            Game.update({_id: user},
                {
                    $set :
                    {
                        temperature: data.temperature,
                        weather: data.weather
                    }
                },
                function(err){
                    if(err) {
                        reject(err);
                    } else {
                        resolve();
                    }
                });
        });
    }

    function getGameByUser(user) {
        return new Promise(function(resolve, reject) {
            Game.findOne({_id: user}, function(err, game) {
                if(err) {
                    reject(err);
                } else {
                    resolve(game);
                }
            });
        });
    }

    function createPlants(user) {
        return new Promise(function(resolve, reject) {
            var emptyPlants = [], i, plant;

            for(i = 0; i < 36; i++) {
                plant = {
                    type: "",
                    growthPoints: 0,
                    lifePoints: 0,
                    isAlive: false,
                    position: i
                };
                emptyPlants.push(plant);
            }
            Game.update({_id: user},
                        {$set: {plants: emptyPlants}},
                        function(err){
                            if(err) {
                                reject(err);
                            }
                            resolve();
                        });
        });
    }

    // Updates the cash by the given value (+/-)
    function updateCash(user, value) {
        return new Promise(function(resolve, reject) {
            Game.update({_id: user},
                        {$inc: {cash: value}},
                        function(err) {
                            if(err) {
                                reject();
                            }
                            resolve();
                        });
        });
    }

    // Creates a basic inventory for the user in database
    function createInventory(user) {
        return new Promise(function (resolve, reject) {
            var standardInventory = PlantProvider.getFilteredPlants();
            Game.update({_id: user}, { $set : { inventory: standardInventory}},
                function(err) {
                    if(err) {
                        reject();
                    }
                    resolve();
                });
        });
    }

    // Updates all plants by an array of plants
    // because we can now simply use the map function of
    // standard JavaScript array
    function updateAllPlants(user, updatedPlants) {
        return new Promise(function(resolve, reject) {
            Game.update({_id: user}, {
                $set: {
                    plants: updatedPlants
                }
            }, function(err) {
                if(err) {
                    return reject(err);
                }
                return resolve();
            });
        });
    }

    // Waters an item on the game field (set its lifePoints to 100)
    function waterItem(user, data) {
        return new Promise(function(resolve, reject) {
            Game.update({_id: user, "plants.position": data.location},
                {
                    $set: {
                        "plants.$.lifePoints": 100
                    }
                },
                function(err) {
                    if(err) {
                        reject(err);
                    }
                    resolve();
                });
        });
    }

    // Updates the inventory and increments/decrements cash
    function updateInventory(user, data) {
        return new Promise(function(resolve, reject) {
            Game.update({_id: user, "inventory.type": data.type},
                {
                    $inc : {
                        "inventory.$.amount": data.amount
                    }
                },
                function(err) {
                    if(err) {
                        reject();
                    }
                    resolve();
                });
        });
    }

    // Plants an item and adds it to the database
    function plantItem(user, data) {
        return new Promise(function(resolve, reject) {
            Game.update({_id: user, "plants.position": data.location},
                {
                    $set: {
                        "plants.$.type": data.type,
                        "plants.$.lifePoints": data.lifePoints,
                        "plants.$.isAlive": true
                    }
                },
                function(err) {
                    if(err) {
                        reject(err);
                    }
                    resolve();
                });
        });
    }

    // Removes planted item from the database
    function removeItem(user, data) {
        return new Promise(function(resolve, reject) {
            Game.update({_id: user, "plants.position": data.location},
                {
                    $set: {
                        "plants.$.type": "",
                        "plants.$.lifePoints": 0,
                        "plants.$.isAlive": false,
                        "plants.$.growthPoints": 0
                    }
                },
                function(err) {
                    if(err) {
                        reject();
                    }
                    resolve();
                });
        });
    }

    function connect() {
        mongoose.connect(mongoUri);
    }

    function connectionSuccess() {
        that.emit("connectionSuccess", {});
    }


    that.updateCash = updateCash;
    that.updateInventory = updateInventory;
    that.createInventory = createInventory;
    that.updateAllPlants = updateAllPlants;
    that.removeItem = removeItem;
    that.waterItem = waterItem;
    that.plantItem = plantItem;
    that.createPlants = createPlants;
    that.getGameByUser = getGameByUser;
    that.updateWeather = updateWeather;
    that.createGame = createGame;
    that.getUser = getUser;
    that.getAllUsers = getAllUsers;
    that.checkCredentials = checkCredentials;
    that.createUser = createUser;
    that.connect = connect;
    that.init = init;
    return that;
}());
