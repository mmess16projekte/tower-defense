var EventEmitter = require("events").EventEmitter;

module.exports = function() {
    var that = new EventEmitter();

    function init(interval) {
        setInterval(function() {
            that.emit("tick", {});
        }, interval);

    }

    that.init = init;
    return that;
};
