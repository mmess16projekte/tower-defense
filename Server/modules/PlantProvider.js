var plants = require("../PlantConfig.json");
module.exports = (function() {
    var that = {};

    // Receive all plants which are in the config file
    function getPlantProperties(type) {
        var i;
        for(i = 0; i < plants.plants.length; i++) {
            if(plants.plants[i].type == type) {
                return plants.plants[i];
            }
        }
        return null;
    }

    // This function generates the needed properties to insert
    // the plant array into an empty inventory
    function getFilteredPlants() {
        var filteredPlants = plants.plants.map(function(o) {
            var f = {};
            if(!o.initAmount) {
                f.amount = 0;
            } else {
                f.amount = o.initAmount;
            }

            f.type = o.type;
            return f;
        });
        return filteredPlants;
    }

    that.getFilteredPlants = getFilteredPlants;
    that.getPlantProperties = getPlantProperties;
    return that;
})();
