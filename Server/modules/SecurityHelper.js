var crypto = require("crypto");
var njwt = require("njwt");

module.exports = (function() {
    var that = {},
        appSecret;

    // Init service with global appSecret
    function init(secret) {
        appSecret = secret;
    }

    // Create a hashed password
    function createHash(password) {
        return crypto.createHash("sha256").update(password).digest("hex");
    }

    // Check if password hashses equal
    function verifyHash(password, hash) {
        return hash === createHash(password);
    }

    // Verify an JWT
    function verifyToken(token) {
        var verifiedToken, response = {};
        try {
            verifiedToken = njwt.verify(token, appSecret);
            response.isValid = true;
            response.user = verifiedToken.body.sub;
            return response;
        } catch(e) {
            response.isValid = false;
            return response;
        }
    }

    // Generate token
    function createToken(user) {
        var jwt = njwt.create({sub: user}, appSecret);
        // Token will be valid for 7 days
        jwt.body.exp += (60*60*24*7);
        return jwt.compact();
    }

    function decodeBasicAuth(header) {
        var credentials =  {},
            data;

        if(header.includes("Basic")) {
            // Remove Basic Header
            data = header.replace("Basic", "");
            // Remove spaces
            data = data.replace(/\s+/, "");
            // Decode base64
            data = new Buffer(data, "base64").toString();
            // Split at :
            if(!data.includes(":")) {
                return null;
            }
            data = data.split(":");
            credentials.user = data[0];
            credentials.password = data[1];
            return credentials;
        } else {
            return null;
        }
    }

    // Decode Bearer Authentication (simple split)
    function decodeBearerAuth(header) {
        var token = header.replace("Bearer","");
        token = token.replace(/\s+/, "");
        return token;
    }

    that.decodeBearerAuth = decodeBearerAuth;
    that.decodeBasicAuth = decodeBasicAuth;
    that.verifyToken = verifyToken;
    that.createToken = createToken;
    that.createHash = createHash;
    that.verifyHash = verifyHash;
    that.init = init;
    return that;
})();
