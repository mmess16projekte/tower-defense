var mongoose = require("mongoose");

var userSchema = mongoose.Schema({
    _id : String,
    password : String,
    mail : String,
    location : {
        lat : Number,
        lon : Number
    },
    city: String,
    cityId: Number
});

module.exports = userSchema;
