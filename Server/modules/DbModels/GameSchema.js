var mongoose = require("mongoose");

var gameSchema = mongoose.Schema({
    _id : String,
    timestamp : {type: Date, default: Date.now},
    cash: {type: Number, default: 100},
    weather: {type: String, default: "sunny"},
    temperature: {type: Number, default: 0},
    inventory: [],
    plants: []
});

module.exports = gameSchema;
